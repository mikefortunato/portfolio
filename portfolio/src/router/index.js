import {
    createRouter,
    createWebHistory
} from 'vue-router'
import Home from '../views/Home.vue'

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/blog',
        name: 'Blog',
        component: () => import( /* webpackChunkName: "blog" */ '../views/Blog.vue')
    },
    {
        path: '/projects',
        name: 'Projects',
        component: () => import( /* webpackChunkName: "projects" */ '../views/Projects.vue')
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router