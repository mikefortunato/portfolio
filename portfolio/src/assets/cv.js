export default {
  experience: [{
      title: "Data Scientist",
      subheader: "Novartis, Cambridge MA",
      start: "Nov 2020",
      end: "present",
      content: `As part of the computer aided drug design group I employ data
              science and machine learning techniques to make predictions about
              chemistry - anything from property prediction to synthesis planning.`
    },
    {
      title: "Postdoctoral Researcher",
      subheader: "MIT, Cambridge MA",
      start: "Oct 2018",
      end: "Oct 2020",
      content: `I built and trained neural networks using Python tools like
            tensorflow and keras to predict candidate precursors for target
            organic molecules. Specifically, my focus was on improving ML predictions for reaction types in low data regimes. These predictions can be chained together to
            create full retrosynthetic trees that end in cheap, buyable starting
            materials. This was part of an on-going project as part of the
            <a href="https://mlpds.mit.edu" target="_blank">MLPDS</a> at MIT.`
    },
    {
      title: "Data Science Contractor",
      subheader: "Whisker Labs, Maryland",
      start: "Aug 2018",
      end: "Sep 2018",
      content: `I consulted for Whisker Labs yielding insights from real-time sensor
          data using supervised and unsupervised machine learning techniques.
          This short consulting stint resulted in an interactive dashboard
          deliverable enabling data exploration for C-level executives.`
    },
    {
      title: "The Data Incubator Fellow",
      subheader: "The Data Incubator, Washington DC",
      start: "Jun 2018",
      end: "Aug 2018",
      content: `<a href="https://www.thedataincubator.com/" target="_blank">The Data Incubator</a> is a data science bootcamp fellowship where I learned a variety of machine learning fundamentals and applications. If you're interested in an extremely efficient modern educational environment and machine learning it's absoultely worth applying for the fellowship. My final project during the fellowship was a data-driven nba wager prediction engine called <a href="http://bet-smart.herokuapp.com/" target="_blank">Bet Smart</a>.`
    },
    {
      title: "HPCMP Intern",
      subheader: "Army Research Lab, Aberdeen MD",
      start: "Jun 2017",
      end: "Aug 2017",
      content: `I participated in the High Performance Computing Modernization Program at the Army Research Lab at Aberdeen Proving Grounds. I worked on understanding microstructure heterogeneity in energetic materials using molecular simulation techniques and how it can impact sensitivity and performance. My research involved post processing of billion particle molecular systems and resulted in a python tool leveraging mpi to efficiently analyze and filter such large systems into tractable, visualizable data sets. <a href="https://drive.google.com/open?id=1YvtVxhVcJDDwsIhhVKIOix0czhs3jbK5" target="_blank">[poster]</a>`
    }
  ],
  education: [{
      title: "University of Florida",
      subheader: "Ph.D.",
      start: "Aug 2015",
      end: "May 2018",
      content: `Computational Chemistry<br>
      <b>Dissertation</b>: Developing Software to Assess the Viability of Molecular Mechanics Force Fields for the Prediction of Polymer Properties<br>
      <b>Software release</b>: <a href="https://pysimm.org" target="_blank">pysimm</a>`
    },
    {
      title: "Pennsylvania State University",
      subheader: "M.S.",
      start: "Aug 2012",
      end: "May 2015",
      content: `Computational Materials Science<br>
<b>Thesis</b>: Immunoglobulin G: Solution Dynamics, Carbohydrate Structure, and Self-association From Atomistic and Coarse-grained Simulations <a href="https://etda.libraries.psu.edu/catalog/24261" target="_blank">[link]</a>`
    },
    {
      title: "Pennsylvania State University",
      subheader: "B.S.",
      start: "Aug 2008",
      end: "May 2012",
      content: `Major: Materials Science and Engineering<br>
Minor: Chemistry`
    }
  ],
  skills: [{
    title: "",
    subheader: "Programming Languages, Environments & Tools",
    content: `<b>Software Development</b> | git • docker • CI/CD<br><br>
    <b>Python</b> | numpy • pandas • sklearn • tensorflow • pytorch • mpi4py<br><br>
    <b>Web Development</b> | flask • fastapi • vue • js • html5 • css<br><br>
    <b>Cloud Computing</b> | GCP • AWS<br><br>
    <b>Molecular Simulation</b> | pysimm • lammps • amber • ovito`
  }],
  interests: [{
    title: "",
    subheader: "",
    content: `Apart from science, I'm an avid basketball fan. I really enjoy 
    trying to hone some of my programming and web development skills by 
    building tools and websites related to basketball.<br><br>
    You can see a few I've deployed in my projects section. Other than
    following my beloved 76ers and pretending to be Steph Curry in pickup
    basketball games, I enjoy watching science fiction tv shows, trying to
    improve my chess game and cooking!`
  }]
};