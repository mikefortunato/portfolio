export default [
  {
    title: "ASKCOS",
    links: ["https://askcos.mit.edu", "https://github.com/ASKCOS/ASKCOS"],
    description: "During my time as a post-doc at MIT, I contributed to the ASKCOS synthesis planning project from both the software engineering and the scientific sides of things. I helped convert the academic, monolithic application into a microservice architecture to help deploy the application in industrial settings. I also helped build a set of API endpoints for machine learning predictions, and built a Vue frontend to consume those APIs. The frontend included a new Interactive Path Planner for chemist-guided retrosynthetic planning. From the scientific side of things, I developed a data-augmentation and pre-training workflow to help template-based retrosynthetic neural networks better learn the context for rare template classes."
  },
  {
    title: "Bet Smart",
    links: ["http://bet-smart.herokuapp.com/"],
    description: "Bet Smart was the deliverable from my participation in The Data Incubator. It attempts to predict nba wager outcomes, such as which team will win, which team will cover the spread, and if the two teams will hit the over. It involved curating game logs from over 30 years of nba seasons, featurizing statistics into categorized descriptors, and training various machine learning models to predict the outcomes. While it doesn't achieve any ground-breaking performance, it serves as a useful tool to help inform decisions based on data-driven results!"
  },
  {
    title: "Python Simulation Interface for Molecular Modeing",
    links: ["https://pysimm.org/", "https://github.com/polysimtools/pysimm"],
    description: "pysimm is a set of python tools to cover the entire workflow of molecular simulations. From structure creation to force field assignment to execution using lammps to post processing, pysimm is designed to help you never leave a python environment. Abstract away cryptic syntax to assemble simulation workflows with building blocks, for example, to perform iterative molecular dynamics-monte carlo simulations."
  },
]